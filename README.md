# SaMoCon - ATSAMV71 Motion Controller

An open-source and open-hardware motion controller which originated as a bachelor thesis project at FEE CTU in Prague.
Supported and funded by
- PiKRON s.r.o.
- Ústav teorie informace a automatizace, Akademie věd České republiky (Institute of Information Theory and Automation, Czech Academy of Sciences)

This motion controller is meant to be used as a rapid prototyping platform used in research or early stages of development. The controller is suitable for automatically generated code running on a powerful ARM Cortex M7 core with a NuttX RTOS. As a code generator for the control applications, MATLAB/Simulink or the open-source pysimCoder suite can be used. In this repository, the pysimCoder suite is considered.

# Hardware
See the `hw` directory. It contains the KiCAD projects for the MCU board, the power-stage board, and an add-on board for the driving of a piezo actuator and the strain gauge sensor measurement. A KiCAD PCB template for the add-on board is present.

# Software
The `sw` repository only contains an OpenOCD flashing script for the ATSAMV71.

See the `boards/arm/samv7/samocon` directory at the `samocon-branch` branch in the [zdebanos NuttX GitHub](https://github.com/zdebanos/nuttx) repository with the BSP under development, as well as the [zdebanos NuttX Apps GitHub](https://github.com/zdebanos/nuttx-apps) repository at the `samocon-branch`.

# Control applications
See the `control` directory. The directory contains only `pysimCoder` `.dgm` (diagram) files alongside the description of each file.

# Some Photos
SaMoCon with a PMSM:
![samocon-pmsm](assets/motorconnected.webp)

PysimCoder PMSM Control application running on SaMoCon. Parameter tuning over TCP/IP and data acquisition over UDP/IP.

![setup](assets/setup.webp)

SaMoCon HW:

![stackup](assets/boardstack.webp)