#!/bin/sh

if [ $# -ne 1 ]; then
	echo "Usage: samocon-flash.sh [bin name]"
	exit 1
fi

prog_name=$1

openocd -f interface/stlink.cfg -f target/atsamv.cfg \
	-c "set CHIPNAME atsamv71q21; reset_config none separate;
	    program $prog_name 0x400000; reset; exit"

