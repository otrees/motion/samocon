#!/bin/sh

if [ $# -ne 1 ]; then
	echo "$(basename $0): [bin name]"
	echo "  This script manipulates the bin file in such a way"
	echo "  it is compatible with nxboot."
	echo "  The script is taken from nuttx-apps/boot/nxboot/tools."
	exit 1
fi

scriptdir=$(cd "$(dirname "$0")"; pwd)
prog_name=$1

if ! [ -f $prog_name ]; then
	echo "The given file does not exist"
	exit 1
fi

progdir=$(cd "$(dirname "$prog_name")"; pwd)

# call the Python script which modifies
# the binary for the nxboot bootloader
python3 $scriptdir/nximage.py \
	--version "1.0.0" \
        --header_size 0x200 \
	--primary \
	$prog_name $progdir/image.img

openocd -f interface/stlink.cfg -f target/atsamv.cfg \
	-c "set CHIPNAME atsamv71q21; reset_config none separate;
	    program image.img 0x420000; reset; exit"

rm $progdir/image.img

