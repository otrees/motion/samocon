#!/bin/sh

openocd -f interface/stlink.cfg -f target/atsamv.cfg \
	-c "set CHIPNAME atsamv71q21; reset_config none separate;
	    init; reset; exit"

