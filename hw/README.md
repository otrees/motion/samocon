# SaMoCon - Hardware related stuff
For licensing and copyright, see `LICENSE.txt`.
The implementation consists of two main parts, one relating the microcontroller part using Microchip's ATSAMV71Q21 and the second one relating the power driver. To see the detailed description of used hardware and components, see the [HW Specification wiki](https://gitlab.fel.cvut.cz/otrees/motion/samocon/-/wikis/HW_Spec).

## MCU board
See `parts.ods` for the detailed pinout on ATSAMV71Q21. Kicad project is in the `mcu` directory. The board has been designed to control 8 motor phases which is sufficient for 2 BLDC motors (6 phases used) and 2 stepper motors (all phases used).

![mcuboard](assets/mcu.webp)

### PWMs
The MCU incorporates two PWM peripherals (PWM0, PWM1), each one containing 4 complementary H and L channels. For future usage, both channels have been routed to support various gate drivers, but in our case only H channel is needed for IFX007 control. L channel will be used as GPIO for setting the half-bridges into high impedance state.

However, due to pin collisions, channel n.2 of PWM1 has only L channel routed. It should be possible to set this L channel to act as a H channel, which will be used in our case. If using complementary signals is desired, it's still possible to use 7 channels in total.

### Motor feedback
- 2 quadrature encoders (A + B + index + mark (GPIO))
- 2 connectors for 3 Hall sensor inputs for each motor

### Current measurement
Peripherals AFEC0 and AFEC1 have been used. The MCU board has total of 8+11 channels routed, 3 channels are reserved for additional usage with addon board, leaving 8+8 channels for measuring 4 pairs of currents for each motor.

### Connectivity
- Ethernet with KSZ8081 PHY
- USB-C - OTG usage is desired
- simple UART for terminal usage
- RS232 / RS485 with galvanic isolation
- SPI with 3 chip selects
- 2x CAN FD with galvanic isolation
- SWD connector for flashing and debugging

### Flashing
- STLink v2 is required,
- The flashing `samocon-flash.sh` script using `openocd` is located in the `sw/openocd-flashing` directory.

## Power board
See the `power` directory for Kicad project of the first generation power-stage board. Infineon IFX007 half-bridge switches are used.

![powerboard](assets/power.webp)
