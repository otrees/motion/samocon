Copyright Štěpán Pressl 2024.

This repository describes Open Hardware and is licensed under the CERN-OHL-W v2

You may redistribute and modify this documentation and make products
using it under the terms of the CERN-OHL-W v2 (https:/cern.ch/cern-ohl).
This documentation is distributed WITHOUT ANY EXPRESS OR IMPLIED
WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY
AND FITNESS FOR A PARTICULAR PURPOSE. Please see the CERN-OHL-W v2
for applicable conditions.
Should You produce hardware based on these Sources, you can do so
under the terms specified in the section 4.1 of the CERN-OHL-W v2 license.
