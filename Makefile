default: all

.PHONY: default all nuttx-build omk-apps sw

.NOTPARALLEL:

nuttx-build:
	make -C sw/nuttx-build

nuttx-nxboot: nuttx-build
	make -C sw/nuttx-nxboot

omk-apps:
	make -C sw/nuttx-omk

sw: nuttx-build nuttx-build nuttx-nxboot omk-apps

all: sw

clean:
	rm -f sw/nuttx-omk/config.omk-default
	make -C sw/nuttx-omk clean
	make -C sw/nuttx-build clean
