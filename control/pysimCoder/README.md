# Control applications using pysimCoder

The guide on how to setup and generate code using pysimCoder for the NuttX target is shown [here](https://gitlab.fel.cvut.cz/otrees/motion/samocon/-/wikis/PysimCoder-setup-with-NuttX-and-code-generation-for-SaMoCon). The resulting NuttX binary with the pysimCoder application must be flashed using the script located in the `sw/openocd-flashing` directory.

# Projects
## Piezoactuator open-loop control
Located in `piezo-movement`. Tested with the Physik Instrumente P-871-140 ([datasheet](https://www.pi-usa.us/fileadmin/user_upload/pi_us/files/product_datasheets/P871_Piezo_Bimorph_Bender.pdf)) piezoactuator. Normally, the piezoactuator is controlled in a linear sense. Since SaMoCon produces outputs PWM voltage, an output LC filter is required.

The pysimCoder diagram is used to change the duty of the PWM output. With the help of the Silicon Heaven protocol, the piezoactuator can be conveniently controlled over TCP/IP.

## PMSM Open Loop Control with ADC Measurement
Located in `pmsm-calib-w-adc`. The diagram generates an electrical angle with a constant angular velocity ($\varphi = \omega t$). The motor is controlled in a synchronous way ($d \neq 0,\, q = 0$). The values $d, q$ and $\varphi$ are passed to Inverse Park and Inverse Clarke blocks, which generates $a, b, c$ actions, applied to a PWM NuttX block. The example measures reads ADC data, the data corresponds to the LS shunt currents. The data is then sent over UDP/IP to a target IP address.

This purpose is to try out the readings from the ADC are OK.
  
## PMSM Open Loop Control with Electrical Angle Estimation
Located in `pmsm-calib`. The open-loop control is the same as in `pmsm-calib-w-adc`. In this example, readings from Hall sensors (3 GPIO inputs) and IRC (incrementary rotary encoder) are made. These readings are passed to a PMSM Align block which estimates the electrical angle. The estimated electrical angle and true electrical angle is sent over UDP/IP to a target IP addresss.

The sent data can be visualised on a target PC. The purpose of this diagram is to tune the `Angle Offset` parameter (can be made conveniently over Silicon Heaven) in the PMSM Align block.

## PMSM Closed Loop Non-FOC Control

## PMSM Open Loop Field Oriented Control